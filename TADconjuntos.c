#include <stdio.h>
#include <stdlib.h>
#include "TADconjuntos.h"

void makeset(int numeroconjuntos,int *vet)
{
	int x;
	for(x=1;x<=numeroconjuntos;x++)
	{
		vet[x]=x;
		peso[x]=0;
	}
}

void uniao(int x, int y, int *vet)
{
	x=findset(x,vet);
	y=findset(y,vet);
	if(peso[y]==peso[x])
	{
		peso[y]=peso[x]+1;
	}
	if(peso[x]>peso[y])
	{
		vet[y]=x;
	}
	if(peso[y]>peso[x])
	{
		vet[x]=y;
	}
}

int findset(int x, int *vet)
{
	if(vet[x]==x)
	{
		return x;
	}
	else
	{
			vet[x]=findset(vet[x],vet);
			return vet[x];
	}
}

void impressao(int *vet,int numeroconjuntos)
{
	int i;
	for(i=1;i<=numeroconjuntos;i++)
	{
		printf("\n%i\n",vet[i]);
	}
}
