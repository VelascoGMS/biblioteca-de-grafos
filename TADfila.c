#include <stdio.h>
#include <stdlib.h>
#include "TADfila.h"

struct fila *criafila()
{
	struct fila *f=(struct fila*)malloc(sizeof(struct fila));
	f->ini=NULL;
	f->fim=NULL;
	return f;
}

void inserefila(struct fila *f, int valor)
{
	struct lista1 *n=(struct lista1*)malloc(sizeof(struct lista1));
	n->valor=valor;
	n->prox=NULL;
	if(f->fim!=NULL)
	{
		f->fim->prox=n;
	}
	if(f->fim==NULL)
	{
		f->ini=n;
	}
	f->fim=n;
}

int retirafila(struct fila *f)
{
	struct lista1 *aux;
	int valor=0;
	if(f->ini==NULL)
	{
		printf("\nfila vazia\n");
		return 0;
	}
	aux=f->ini;
	valor=aux->valor;
	f->ini=aux->prox;
	if(f->ini==NULL)
	{
		f->fim==NULL;
	}
	free(aux);
	return valor;
}

void libera(struct fila *f)
{
	struct lista1 *q=f->ini;
	while(q!=NULL)
	{	
		struct lista1 *aux=q->prox;
		free(q);
		q=aux;
	}
	free(f);
}

		
