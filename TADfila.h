
struct lista1
{
	int valor;
	struct lista1 *prox;
};

struct fila
{
	struct lista1 *ini;
	struct lista1 *fim;
};

struct fila *criafila();
void inserefila(struct fila *f, int valor);
int retirafila(struct fila *f);
void libera(struct fila *f);
