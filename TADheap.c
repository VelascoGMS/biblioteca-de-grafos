#include <stdio.h>
#include <stdlib.h>
#include "TADheap.h"
#include "bibliotecagraph.h"

void criaheap(int tamanho,int peso,int partida, int chegada)
{
	heap[tamanho-1]=NULL;
	heap[tamanho-1]=(Heap*)malloc(sizeof(Heap));
	heap[tamanho-1]->partida=partida;
	heap[tamanho-1]->chegada=chegada;
	heap[tamanho-1]->peso=peso;
	if(tamanho>0)
	{
		chocolate(tamanho);
	}
}

void imprimir(int tamanho)
{
	int y;
	for(y=0;y<tamanho;y++)
	{
		printf("\nPartida:%i --->Peso:%i --->Chegada:%i\n",heap[y]->partida,heap[y]->peso,heap[y]->chegada);
	}
}

void siftdown(int tamanho)
{
	int aux=0,aux2=0,aux3=0,tam=0;
	if(tamanho==0 || tamanho==1)
	{
		return;
	}
	if(tamanho>1)
	{
		while(tam!=tamanho-1)
		{
			if(heap[tam]->peso > heap[tam+1]->peso)
			{
				aux=heap[tam]->peso;
				heap[tam]->peso=heap[tam+1]->peso;
				heap[tam+1]->peso=aux;
				aux2=heap[tam]->partida;
				heap[tam]->partida=heap[tam+1]->partida;
				heap[tam+1]->partida=aux2;
				aux3=heap[tam]->chegada;
				heap[tam]->chegada=heap[tam+1]->chegada;
				heap[tam+1]->chegada=aux3;
			}
			tam++;
		}
	}
}

void chocolate(int tamanho)
{
	int aux=0,aux2=0,aux3=0, tam=tamanho-1;
	for(tam;tam!=0;tam--)
	{
		if(heap[tam-1]->peso > heap[tam]->peso)
		{
			aux=heap[tam]->peso;
			heap[tam]->peso=heap[tam-1]->peso;
			heap[tam-1]->peso=aux;
			aux2=heap[tam]->partida;
			heap[tam]->partida=heap[tam-1]->partida;
			heap[tam-1]->partida=aux2;
			aux3=heap[tam]->chegada;
			heap[tam]->chegada=heap[tam-1]->chegada;
			heap[tam-1]->chegada=aux3;
		}
	}
}

Heap *getraiz(int tamanho)
{
	Heap *aux;
	aux=(Heap*)malloc(sizeof(Heap));
	aux->peso=heap[0]->peso;
	aux->partida=heap[0]->partida;
	aux->chegada=heap[0]->chegada;
	return aux;
}

void deleteHeap(int tamanho)
{
	heap[0]->peso=heap[tamanho-1]->peso;
	heap[0]->partida=heap[tamanho-1]->partida;
	heap[0]->chegada=heap[tamanho-1]->chegada;
	free(heap[tamanho-1]);
	siftdown(tamanho);
}

Heap *buscaheap(int tamanho,int buscado)
{
	Heap *aux=(Heap*)malloc(sizeof(Heap));
	int i;
	for(i=0;i<tamanho;i++)
	{
		if(heap[i]->partida==buscado)
		{
			aux->peso=heap[i]->peso;
			aux->partida=heap[i]->partida;
			aux->chegada=heap[i]->chegada;
			return aux;
		}
	}
	return NULL;
}
