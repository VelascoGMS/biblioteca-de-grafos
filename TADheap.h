struct heap
{
  int partida;
  int chegada;
  int peso;
};
typedef struct heap Heap;

Heap *heap[10000];

void criaheap(int tamanho,int peso, int partida, int chegada);
void imprimir(int tamanho);
void chocolate(int tamanho);
void deleteHeap(int tamanho);
Heap *getraiz(int tamanho);
void siftdown(int tamanho);
Heap *buscaheap(int tamanho,int buscado);
