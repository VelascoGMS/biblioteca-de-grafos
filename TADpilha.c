#include <stdio.h>
#include <stdlib.h>
#include "TADpilha.h"


Pilha *criapilha()
{
	Pilha *p=(Pilha*)malloc(sizeof(Pilha));
	p->prim=0;
	return p;
}

void inserepilha(Pilha*p,int valor)
{
	Lista *n=(Lista*)malloc(sizeof(Lista));
	n->valor=valor;
	n->prox=p->prim;
	p->prim=n;
}

int retirapilha(Pilha *p)
{
	Lista *aux;
	int valor;
	if(p->prim==NULL)
	{
		printf("\nPilha vazia\n");
		exit(1);
	}
	aux=p->prim;
	valor=aux->valor;
	p->prim=aux->prox;
	free(aux);
	return valor;
}

void liberapilha(Pilha *p)
{
	Lista *q=p->prim;
	while(q!=NULL)
	{	
		Lista *aux=q->prox;
		free(q);
		q=aux;
	}
	free(p);
}
