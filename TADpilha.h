

struct lista 
{
	int valor;
	struct lista *prox;
};
typedef struct lista Lista;

struct pilha
{
	Lista *prim;
};
typedef struct pilha Pilha; 

Pilha *criapilha();
void inserepilha(Pilha*p,int valor);
int retirapilha(Pilha *p);
void liberapilha(Pilha *p);
