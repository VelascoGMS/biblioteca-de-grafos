#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bibliotecagraph.h"
#include "TADpilha.h"
#include "TADfila.h"
#include "TADconjuntos.h"
#include "TADheap.h"


Grafo* carrega(char *arquivo)
{
	int tamanho=0,x,origem=0,destino=0,peso=0;
	char y[10000];
	char z;
	FILE *fp=fopen(arquivo,"r");
	if((fp=fopen(arquivo,"r"))==NULL)
	{
		printf("\n\nERRO AO ACESSAR OS DADOS\n\n");
		getchar();
		getchar();
		return NULL;
	}
	fscanf(fp,"%i",&tamanho);
	Grafo *g=NULL;
	g=criagrafo(tamanho);
	x=0;
	while((z=fgetc(fp))!=EOF)
	{
		if(z!='\n')
		{
			y[x]=z;
			x++;
		}
		else
		{
			y[x]='\0';
			if(x>0)
			{
				origem=atoi(strtok(y,";"));
				destino=atoi(strtok(NULL,";"));
				peso=atoi(strtok(NULL,";"));
				g=insere(origem,g);
				g=adj(g,origem,destino,peso);
			}
			x=0;
			y[x]='\0';
		}

	}
	fclose(fp);
	printf("\nGrafo criado com sucesso\n");
	return g;
}

Grafo * criagrafo(int tamanho)
{
	Grafo *n=(Grafo*)malloc(sizeof(Grafo));
	n->tamanho=tamanho;
	n->ListaNodos=NULL;
	return n;
}

Grafo *insere(int origem, Grafo *g)
{
	Vertice *novo;
	Vertice *aux;
	int qtd=0;
	qtd=buscaaux(g,origem);
	if(qtd<1)
	{
	novo=vertice(origem);
	if(g->ListaNodos==NULL)
	{
		g->ListaNodos=novo;
	}
	else
	{
		aux=g->ListaNodos;
		while(aux->prox!=NULL)
		{
			aux=aux->prox;
		}
		aux->prox=novo;
	}
	}
	else
	{
		return g;
	}
	return g;
}

Vertice *vertice(int origem)
{
	Vertice *n=(Vertice*)malloc(sizeof(Vertice));
	n->chave=origem;
	n->prox=NULL;
	n->adjacencia=NULL;
	return n;
}

Aresta *criaaresta(int destino,int peso,int origem)
{
	Aresta *n=(Aresta*)malloc(sizeof(Aresta));
	n->chave=destino;
	n->peso=peso;
	n->prox=NULL;
  n->chavepartida=origem;
	return n;
}


Grafo *adj(Grafo *g,int origem,int destino,int peso)
{
	Vertice *ini=busca(g,origem);
	if(ini==NULL)
	{
		printf("\nVertice não existe\n");
		return g;
	}
	Aresta *fim=criaaresta(destino,peso,origem);
	if(ini->adjacencia==NULL)
	{
		ini->adjacencia=fim;
	}
	else
	{
		Aresta *aux=ini->adjacencia;
		while(aux->prox!=NULL)
		{
			aux=aux->prox;
		}
		aux->prox=fim;
	}
	return g;
}

Vertice * busca(Grafo *g,int origem)
{
	Vertice *aux=g->ListaNodos;
	for(aux;aux!=NULL;aux=aux->prox)
	{
		if(aux->chave==origem)
		{
			return aux;
		}
	}
	return NULL;
}

int buscaaux(Grafo *g, int origem)
{
	int x=0;
	Vertice *aux=g->ListaNodos;
	while(aux!=NULL)
	{
		if(aux->chave==origem)
		{
			x++;
		}
		aux=aux->prox;
	}
		return x;
}

void imprimi(Grafo *g)
{
  Vertice *aux;
  Aresta *temp;
  printf("\nTamanho do grafo: %i\n",g->tamanho);
  for(aux=g->ListaNodos;aux!=NULL;aux=aux->prox)
	{
		for(temp=aux->adjacencia;temp!=NULL;temp=temp->prox)
		{
			printf("\nOrigem:%d ---->Peso:%d ---->Destino:%d\n",temp->chavepartida,temp->peso,temp->chave);
		}
	}
}



void buscaprofundidade(Grafo *g,int inicio)
{
	int x,set=1,retirado=0,y,solucao[g->tamanho];
	Pilha *p=criapilha();
	for(x=1;x<=g->tamanho;x++)
	{
		solucao[x]=0;
	}
	solucao[inicio]=set;
	set++;
	Vertice *aux=busca(g,inicio);
	if(aux!=NULL)
	{
		Aresta *temp=aux->adjacencia;
		for(temp;temp!=NULL;temp=temp->prox)
		{
			inserepilha(p,temp->chave);
		}
	}
	for(p;p->prim!=NULL;)
	{
		retirado=retirapilha(p);
		if(solucao[retirado]==0)
		{
			solucao[retirado]=set;
			set++;
			Vertice *ttemp=busca(g,retirado);
			if(ttemp!=NULL)
			{
				Aresta *aaux=ttemp->adjacencia;
				for(aaux;aaux!=NULL;aaux=aaux->prox)
				{
					inserepilha(p,aaux->chave);
				}
			}
		}
	}
	for(y=1;y<=g->tamanho;y++)
	{
		printf("\nPosicao: %i   Vertice visitado: %i\n",solucao[y],y);
	}
	liberapilha(p);
}

void buscalargura(Grafo *g,int inicio)
{
	int x,set=1,retirado=0,y,solucao[g->tamanho];
	struct fila *f=criafila();
	for(x=1;x<=g->tamanho;x++)
	{
		solucao[x]=0;
	}
	solucao[inicio]=set;
	set++;
	Vertice *aux=busca(g,inicio);
	if(aux!=NULL)
	{
		Aresta *temp=aux->adjacencia;
		for(temp;temp!=NULL;temp=temp->prox)
		{
			inserefila(f,temp->chave);
		}
	}
	for(f;f->ini!=NULL;)
	{
		retirado=retirafila(f);
		if(solucao[retirado]==0)
		{
			solucao[retirado]=set;
			set++;
			Vertice *ttemp=busca(g,retirado);
			if(ttemp!=NULL)
			{
				Aresta *aaux=ttemp->adjacencia;
				for(aaux;aaux!=NULL;aaux=aaux->prox)
				{
					inserefila(f,aaux->chave);
				}
			}
		}
	}
	for(y=1;y<=g->tamanho;y++)
	{
		printf("\nPosicao: %i   Vertice visitado: %i\n",solucao[y],y);
	}
	libera(f);
}

void kruskal(Grafo *g)
{
	int tam=g->tamanho,set=0,z,indice=0,tamaux=0;
	int vertices[tam];
	Heap *solucao[tam],*aresta;
	Vertice *aux=g->ListaNodos;
	for(aux;aux!=NULL;aux=aux->prox)
	{
		vertices[set]=aux->chave;
		Aresta *temp=aux->adjacencia;
		for(temp=aux->adjacencia;temp!=NULL;temp=temp->prox)
		{
			tamaux++;
			criaheap(tamaux,temp->peso,temp->chavepartida,temp->chave);
		}
		set++;
	}
	makeset(tam,vertices);
	while(tamaux > 0)
	{
		aresta=getraiz(tamaux);
		deleteHeap(tamaux);
		if(findset(aresta->partida,vertices)!=findset(aresta->chegada,vertices))
		{
				uniao(aresta->chegada,aresta->partida,vertices);
				solucao[indice]=aresta;
				indice++;
		}
		tamaux--;
	}
	for(z=0;z<indice;z++)
	{
		if(solucao[z]->peso!=0)
		{
		printf("\nPartida:%i ----->Peso:%i ----->Chegada:%i\n",solucao[z]->partida,solucao[z]->peso,solucao[z]->chegada);
		}

	}
}

void Prim(Grafo *g)
{
	int tam=g->tamanho;
	int inicio=0,set=0,indice=0,z,cont=0;
	int vertices[tam];
	Aresta *solucao[tam];
	printf("\nDigite o nodo pelo qual deseja iniciar:  ");
	scanf("%i",&inicio);
	Vertice *aux=g->ListaNodos;
	for(aux;aux!=NULL;aux=aux->prox)
	{
		vertices[set]=aux->chave;
		set++;
	}
	makeset(tam,vertices);
	Aresta *ref=buscaaresta(g,inicio);
	solucao[indice]=ref;
	indice++;
	Vertice *buscado=busca(g,inicio);
	if(buscado!=NULL)
	{
		Aresta *temp=buscado->adjacencia;
		for(temp;temp!=NULL;temp=temp->prox)
		{
			cont++;
			criaheap(cont,temp->peso,temp->chavepartida,temp->chave);
		}
	}
	imprimir(cont);
	while(cont>0)
	{
		Heap *aresta=getraiz(cont);
		deleteHeap(cont);
		Vertice *nodochegada=busca(g,aresta->chegada);
		if(findset(aresta->partida,vertices)!=findset(aresta->chegada,vertices))
		{
			if(nodochegada!=NULL)
			{
				Aresta *ttemp=nodochegada->adjacencia;
				for(ttemp;ttemp!=NULL;ttemp=ttemp->prox)
				{
					cont++;
					criaheap(cont,aresta->peso,aresta->partida,aresta->chegada);
					uniao(aresta->partida,aresta->chegada,vertices);
					Aresta *arest=buscaaresta(g,aresta->partida);
					solucao[indice]=arest;
					indice++;
				}
			}
		}
		cont--;
	}
	for(z=0;z<indice;z++)
	{
		if((solucao[z]->chavepartida && solucao[z]->chave && solucao[z]->peso)!=(solucao[z]->chavepartida && solucao[z]->chave && solucao[z]->peso))
		{
			printf("\nPartida:%d ----->Peso:%d ----->Chegada:%d \n",solucao[z]->chavepartida,solucao[z]->peso,solucao[z]->chave);
		}
	}
}

Aresta *buscaaresta(Grafo *g,int origem)
{
	Vertice *aux=g->ListaNodos;
	for(aux;aux!=NULL;aux=aux->prox)
	{
		Aresta *aaux=aux->adjacencia;
		for(aaux;aaux!=NULL;aaux=aaux->prox)
		{
			if(aaux->chavepartida==origem)
			{
				return aaux;
			}
		}
	}
	return NULL;
}
