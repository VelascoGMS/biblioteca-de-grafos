#define N 100
struct grafo
{
	struct nodo *ListaNodos;
	int tamanho;
};
typedef struct grafo Grafo;

struct nodo
{
	int chave;
	struct nodo *prox;
	struct aresta *adjacencia;
};
typedef struct nodo Vertice;

struct aresta
{
	int chave;
	struct aresta *prox;
	int peso;
  int chavepartida;
};
typedef struct aresta Aresta;

Grafo* carrega(char *arquivo);
Grafo * criagrafo(int tamanho);
Grafo *insere(int origem, Grafo *g);
Vertice *vertice(int origem);
Aresta *criaaresta(int destino, int peso,int origem);
Grafo *adj(Grafo *g,int origem,int destino,int peso);
Vertice * busca(Grafo *g,int buscado);
int buscaaux(Grafo *g, int origem);
Aresta *buscaaresta(Grafo *g,int origem);
void imprimi(Grafo *g);
void buscaprofundidade(Grafo *g,int inicio);
void buscalargura(Grafo *g,int inicio);
void kruskal(Grafo *g);
void Prim(Grafo *g);
