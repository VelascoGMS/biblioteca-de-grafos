#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bibliotecagraph.h"
#include "TADpilha.h"
#include "TADfila.h"
#include "TADconjuntos.h"
#include "TADheap.h"


int main()
{
	int escolha=0;
	char nome[50];
	Grafo *g=NULL;
	while(1)
	{
		printf("\nEscolha um comando:\n");
		printf("\n(0) Sair");
		printf("\n(1) Cria grafo");
		printf("\n(2) Imprimir grafo\n");
		printf("(3) Busca em profundidade\n");
		printf("(4) Busca em largura\n");
		printf("(5) Algoritmo de Kruskal\n");
		printf("(6) Algoritmo de Prim\n");
		printf("Sua escolha:   ");
		scanf("%i",&escolha);
		if(escolha==1)
		{
			printf("\nDigite o nome do arquivo de entrada:  ");
			scanf("%s",nome);
			g=carrega(nome);
		}
		if(escolha==2)
		{
			imprimi(g);
		}
		if(escolha==3)
		{
			int inicio=0;
			printf("\nDigite o vertice em que a busca em profundidade se originara:  ");
			scanf("%i",&inicio);
			buscaprofundidade(g,inicio);
		}
		if(escolha==4)
		{
			int origem=0;
			printf("\nDigite o vertice em que a busca em largura se originara:  ");
			scanf("%i",&origem);
			buscalargura(g,origem);
		}
		if(escolha==5)
		{
			kruskal(g);
		}
		if(escolha==6)
		{
			Prim(g);
		}
		if(escolha==0)
		{
			return 0;
		}
	}
	return 0;
}
